#include <pthread.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

typedef struct arg_hilo{
	int desde;
	int hasta;
	char *msg;
}argumento_hilo;

void *rutina_hilo(void *args){
	argumento_hilo *p = (argumento_hilo *)args;
	printf("Contando...\n");
	for(int i = p->desde; i < p->hasta; i++){
		printf("Hilo %ld contando %d... %s\n", pthread_self(), i, p->msg);
		sleep(1);
	}

	return (void *)0;
}

void *rutina2(void *args){

	
	argumento_hilo *p = (argumento_hilo *)args;
	int *arr = calloc(10, sizeof(int));
	for(int i = p->desde; i >= p->hasta; i--){
		if(i < 10){
			arr[i] = i;
		}
		printf("Hilo %ld contando %d... %s\n", pthread_self(), i, p->msg);
                sleep(1);
	}
	return arr;
}

int main(){
	unsigned int id = pthread_self();
	printf("Id del hilo principal %d\n", id);

	argumento_hilo arg1;
	arg1.desde = 0;
	arg1.hasta = 20;
	arg1.msg = "hola";

	argumento_hilo *arg2 = malloc(sizeof(argumento_hilo));
	arg2->desde = 15;
	arg2->hasta = 0;
	arg2->msg = "chao";


	pthread_t id_hilo;
	pthread_t id_hilo2;
	pthread_create(&id_hilo, NULL, rutina_hilo, &arg1);

	pthread_create(&id_hilo2, NULL, rutina2, arg2);

	pthread_join(id_hilo, NULL);


	int *valor_devuelto;

	pthread_join(id_hilo2, (void *)&valor_devuelto);

	for(int i = 0; i < 10; i++){
		printf("valor_devuelto[%d] = %d\n", i, valor_devuelto[i]);
	}

	return 0;
}

