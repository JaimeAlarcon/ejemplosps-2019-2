#include <pthread.h>
#include <stdio.h>
#include <unistd.h>

void *rutina_hilo(void *args){

	printf("Contando...\n");
	for(int i = 0; i < 20; i++){
		printf("Hilo %ld contando %d...\n", pthread_self(), i);
		sleep(1);
	}

	return (void *)0;
}

void *rutina2(void *args){
	for(int i = 15; i >= 0; i--){
		printf("Hilo %ld contando %d...\n", pthread_self(), i);
                sleep(1);
	}
	return NULL;
}

int main(){
	unsigned int id = pthread_self();
	printf("Id del hilo principal %d\n", id);

	pthread_t id_hilo;
	pthread_t id_hilo2;
	pthread_create(&id_hilo, NULL, rutina_hilo, NULL);

	pthread_create(&id_hilo2, NULL, rutina2, NULL);

	for (int i = 0; i < 5; i++){
		printf("Hilo principal contando... %d\n", i);
		sleep(1);
	}
	pthread_join(id_hilo, NULL);
	pthread_join(id_hilo2, NULL);
	printf("Los hilos trabajadores terminaron\n");
	return 0;
}

