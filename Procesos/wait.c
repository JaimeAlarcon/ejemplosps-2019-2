#include <stdio.h>
#include <unistd.h>
 #include <sys/types.h>
       #include <sys/wait.h>


int main(){

	pid_t pid = fork();

	int a = 4;
	if(pid == 0){
		a++;
		printf("proceso hijo pid = %d, ppid = %d\n", getpid(), getppid());
		printf("a = %d\n", a);
		//while(1);
		return 27;
	}
	else{
		a--;
		printf("Proceso padre pid = %d\n", getpid());
		printf("a = %d\n", a);
		
		int status;
		while(1);
		//wait(&status);
		//waitpid(pid, &status, 0);
		printf("Valor que retorno el hijo %d\n", WEXITSTATUS(status));
	}
	return 0; 

}
