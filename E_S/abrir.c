 #include <sys/types.h>
       #include <sys/stat.h>
       #include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>


int main(){
	umask(0);
	int fd = open("bitacora.log", O_WRONLY | O_CREAT | O_TRUNC, 0666);

	if(fd < 0){
		perror("error al abrir el archivo");
	}

	char *msg = "este es un texto de prueba\n";

	int escritos = write(fd, msg, strlen(msg));

	if(escritos < 0){
		perror("Error al escribir archivo.");
	}
	char *msg2 = "holaaaaaa\n";
	write(fd, msg2, strlen(msg2));


	long valor = 342423;
	write(fd, &valor, sizeof(valor));
	
	close(fd);


	return 0;
}
