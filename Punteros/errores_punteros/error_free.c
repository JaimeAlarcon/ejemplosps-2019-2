#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int main(){
	unsigned char *p = malloc(10*sizeof(char));

	memset(p, 0xff, 10*sizeof(char));

	printf("p[4] = %x\n", p[4]);

	free(p);

	//
	unsigned char *z = malloc(10*sizeof(char));
	memset(z, 0xAA, 10*sizeof(char));

	printf("z[4] = %x\n", z[4]);
	printf("p[4] = %x\n", p[4]);
}
