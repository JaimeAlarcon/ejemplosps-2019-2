#include <stdio.h>


typedef int (*fn)(int, int);

int suma(int a, int b){
	printf("suma: %d\n", a+ b);
	return 0;
}

int resta(int a, int b){
        printf("resta: %d\n", a-b);
        return 0;
}

void operacion(int a, int b, fn operacion_a_realizar){
	operacion_a_realizar(a, b);
}

int main(){

	operacion(6,7, suma);
	operacion(6,7, resta);

	return 0;
}
