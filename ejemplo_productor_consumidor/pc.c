#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#define  nHilos 6

sem_t principal[nHilos];	//empiezan en 1
sem_t worker[nHilos];	//empiezan en 0


sem_t mutex;
int var;

typedef struct args{
	int id_hilo;
}args_hilos;


void *trabajador(void *args){
	args_hilos *argumento = (args_hilos *)args;
	int id = argumento->id_hilo;
	
	while(1){
		sem_wait(&worker[id]);		//Esperamos que hilo principal nos permita avanzar

		sem_wait(&mutex);
		var += 10;				//variable compartida, proteger con semaforo;
		sem_post(&mutex);

		sem_post(&principal[id]);		//Hilo notifica a main que ya ha terminado esta iteracion.
	}
}






int main(){

	//init semaforos
	for(int i = 0; i < nHilos; i++){
		sem_init(&principal[i], 0, 1);
		sem_init(&worker[i],0, 0);
	}

	sem_init(&mutex,0,1);
	int primera_iteracion = 1;

	pthread_t ids[nHilos];
        args_hilos argumentoHilos[nHilos];
	printf("Iniciando...\n");
	while(1){
		//Todos los hilos worker debieron notificar al hilo
		//main para que continue
		for(int i = 0; i < nHilos; i++){
                	sem_wait(&principal[i]);
		}
		
		//si es primera iteracion, creamos los hilos
		if(primera_iteracion){
			for(int i = 0 ; i < nHilos; i++){
				argumentoHilos[i].id_hilo = i;		//id de hilo
				pthread_create(&ids[i], NULL, trabajador, &argumentoHilos[i]);
			}
			var = 0;
			primera_iteracion = 0;
		}
		else{
			//Mostrar resultado de trabajadores
			printf("Hilos trabajadores terminaron iteracion. Valor de var = %d\n", var);
		}

		//Hilo principal asigna valor nuevo a var
		var += 100;

		//hilo principal notificar a trabajadore que pueden empezar
		 for(int i = 0; i < nHilos; i++){
	                sem_post(&worker[i]);
		}
	 	sleep(1);	 
	
	}

}

